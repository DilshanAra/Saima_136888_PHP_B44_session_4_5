<?php
//to print "" as string manually
echo "how are \"you\"";
//to print '' no need for any special function
echo "<br>how are 'you'<br>";
//to print auto "" withought error on a paragraph use addslashes function
//the output will be auto slashed,run the output as code
$myStr= addslashes('hello "37" saima, hello "37" saima, hello "37" saima, hello "37" saima, hello "37" saima, hello "37" saima, hello "37" saima, hello "37" saima, hello "37" saima, ');
echo $myStr;
echo "<br>hello \"37\" saima, hello \"37\" saima, hello \"37\" saima, hello \"37\" saima, hello \"37\" saima, hello \"37\" saima, hello \"37\" saima, hello \"37\" saima, hello \"37\" saima,";
$myStr= addslashes('<br>hello \37\ saima, hello \'37\' saima, hello "37" saima');
echo $myStr . "<br>";
$str = "Hello world! How R U?<br>";
//to take words in array when space is found, space will not enter array
$array = explode(" ",$str);
print_r($array);
echo"<br>";
//to add spaces in output
$array = implode(" ",$array);
echo($array);
echo"<br>";
//to print html or php codes cmmands as text
$str1 = '<br> means line break';
$str1 = htmlentities($str1);
echo $str1;
echo"<br>";
//triming the spaces from output ltrim & rtrim
$mystr1 ="  Hello World  ";
$mystr1 = ltrim($mystr1);
echo $mystr1;
echo"<br>";
echo rtrim($mystr1);
$mystr1 = ltrim($mystr1);
echo $mystr1;
echo"<br>";

$mystr2 = "\n\n\n\n\n hi there";
$mystr2 = nl2br($mystr2);
echo $mystr2;
echo"<br>";

$main = "helllo star";
$main = str_pad($main,50,"*");
echo $main;
echo"<br>";

$mainrep = "helllo star ";
$mainrep = str_repeat($mainrep,5);
echo $mainrep;
echo"<br>";

$mainreplace = "helllo Saima ";
$mainreplace = str_replace("I","M",$mainreplace);
echo $mainreplace;
echo"<br>";
//to make words first word first char uppercase
 $app = "hello world";
echo ucfirst($app) . "<br>";
//to make every words first char uppercase
echo ucwords($app);

















